# Preaction validation

This is a small validation library.

## Validator Format

A validator should always take a value and return a string. Validity is determined by the returned string's length.

## Usage

Install with yarn or npm.

yarn:

```bash
yarn add @preaction/validation
```

npm:

```bash
npm install --save @preaction/validation
```

import

```javascript
import { emailValidator } from '@preaction/validation'
// or
const emailValidator = require('@preaction/validation').emailValidator
```

use:

```javascript
let errorMessage
let valid

errorMessage = emailValidator('test@example.com')
valid = !errorMessage
console.debug(valid, errorMessage) // true, ''

errorMessage = emailValidator('test@example.')
valid = !errorMessage
console.debug(valid, errorMessage) // false, 'Enter a valid email.'
```
